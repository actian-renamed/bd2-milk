DROP TRIGGER IF EXISTS auto_date_zaswiadczenie;
DROP TRIGGER IF EXISTS auto_date_zgloszenie;
DROP TRIGGER IF EXISTS auto_NULL_id_opinii;

CREATE TRIGGER auto_date_zaswiadczenie AFTER INSERT 
ON Zaswiadczenie
BEGIN
   UPDATE zaswiadczenie
   SET data_wystawienia = datetime('now', 'localtime')
   WHERE id = new.id;
END;

CREATE TRIGGER auto_date_zgloszenie AFTER INSERT 
ON Zgloszenie
BEGIN
   UPDATE zgloszenie
   SET data_zgloszenia = datetime('now', 'localtime')
   WHERE id = new.id;
END;

CREATE TRIGGER auto_NULL_id_opinii AFTER INSERT 
ON Zgloszenie
BEGIN
   UPDATE zgloszenie
   SET id_opinii = NULL 
   WHERE id = new.id;
END;
